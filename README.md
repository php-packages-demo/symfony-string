# [symfony/string](https://packagist.org/packages/symfony/string)

Provides an object-oriented API to strings and deals with bytes, UTF-8 code points and grapheme clusters in a unified way

*  Symfony5: The Fast Track [*Step 13: Managing the Lifecycle of Doctrine Objects*](https://symfony.com/doc/current/the-fast-track/en/13-lifecycle.html)
